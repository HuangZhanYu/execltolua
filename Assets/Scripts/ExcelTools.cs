﻿using System;
using System.Text;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;
using System.IO;
using UnityEngine;
using System.Collections.Generic;

public class ExcelTools
{
    private Dictionary<string, string> defult;//默认的值
    private string defultTableName = "defult"; //默认值table名字
    private string defultTableStr = ""; //默认值table字符串
    /// <summary>
    /// 刷新工作簿
    /// </summary>
    /// <param name="excelFilePath">excel表格路径</param>
    /// <returns></returns>
    public IWorkbook updateWorkBook(string excelFilePath)
    {
        IWorkbook workBook = null;
        ICSharpCode.SharpZipLib.Zip.ZipConstants.DefaultCodePage = Encoding.UTF8.CodePage;
        using (FileStream fileStream = File.Open(excelFilePath, FileMode.Open, FileAccess.Read,FileShare.ReadWrite))
        {
            if (excelFilePath.IndexOf(".xlsx") > 0) // 07以上版本
            {
                workBook = new XSSFWorkbook(fileStream);
            }
            else if (excelFilePath.IndexOf(".xls") > 0) //03版本
            {
                workBook = new HSSFWorkbook(fileStream);
            }
        }
        return workBook;
    }

   /// <summary>
   /// 初始化默认值
   /// </summary>
   /// <param name="sheet"></param>
    private void initDefultValue(ISheet sheet)
    {
        IRow rowValueType = sheet.GetRow(1); //值类型
        IRow rowKey = sheet.GetRow(2); //键
        IRow defultValue = sheet.GetRow(3); //默认值
        defult = new Dictionary<string, string>();
        defultTableStr = "local " + defultTableName + " ={\n\t";
        for (int j = 1; j < rowKey.LastCellNum; j++) // 列
        {
            string key = null; //字段名
            string value = null;//
            string[] cellValue = getCellData(defultValue, rowValueType, rowKey, j);
            key = cellValue[0];
            value = cellValue[1];
            if (string.IsNullOrEmpty(key) && string.IsNullOrEmpty(value))
            {
                break;
            }
            string str = null;
            if (j != rowKey.LastCellNum - 1)
            {
                str = string.Format("{0}={1},\n\t", key, value);
            }else
            {
                str = string.Format("{0}={1},\n", key, value);
            }
            defultTableStr += str;
            defult.Add(key, value);
        }
        defultTableStr += "}\n";
    }

    /// <summary>
    /// 获取单元格的值
    /// </summary>
    /// <param name="targetRow"></param>
    /// <param name="rowValueType"></param>
    /// <param name="rowKey"></param>
    /// <param name="index"></param>
    /// <returns></returns>
    private string[] getCellData(IRow targetRow, IRow rowValueType, IRow rowKey, int index)
    {
        string[] cellValue = new string[2];
        string valueType = null;//值类型
        string key = null; //字段名
        string value = null;//值
        ICell cell = null;//单元格
        cell = rowValueType.GetCell(index); //获取值类型cell
        valueType = getCellValue(cell);

        cell = rowKey.GetCell(index); // 获取key cell
        key = getCellValue(cell);

        cell = targetRow.GetCell(index); //获取值
        value = getCellValue(cell);

        value = parseValue(valueType, value);//解析一下值

        cellValue[0] = key;
        cellValue[1] = value;
        return cellValue;
    }

    /// <summary>
    /// 解析成lua对象
    /// </summary>
    /// <param name="sheet"></param>
    public string parseToLuaObj(ISheet sheet,string jsonPath)
    {
        if (sheet == null)
        {
            return "";
        }
        initDefultValue(sheet);
        string str = defultTableStr+"local " + sheet.SheetName+ "={\n\t";
        int rowCount = sheet.LastRowNum; //最后
        // 0 行描述  1行字段类型 2行字段名 3行默认值
        IRow rowValueType = sheet.GetRow(1); //值类型
        IRow rowKey = sheet.GetRow(2); //键
        for (int i = 4; i <= rowCount; i++) //行
        {
            IRow row = sheet.GetRow(i);//获取行数据
            ICell idCell =  row.GetCell(0);

            string id = getCellValue(idCell);
            if (string.IsNullOrEmpty(id))
            {
                Debug.LogError(">>>id 不能为空");
                continue;
            }
            string result = "";
            if (i!=4)
            {
                result = ",\n\t[" + id + "] = {";
            }
            else
            {
                result = "[" + id + "] = {";
            }
            for (int j = 1; j < rowKey.LastCellNum; j++) // 列
            {
                string key = null; //字段名
                string value = null;//值

                string[] cellValue = getCellData(row, rowValueType, rowKey, j);
                key = cellValue[0];
                value = cellValue[1];

                if (string.IsNullOrEmpty(key) && string.IsNullOrEmpty(value))
                {
                    break;
                }
                if (defult[key] == value) //如果和默认值相同就跳过
                {
                    continue;
                }
                result += string.Format("\n\t\t{0}={1},", key, value);
            }
            result += "\n\t}";
            str += result;
        }
        str += "\n}\n";
        setMetatable(sheet,ref str);
        writeText(jsonPath, str);
        return str;
    }
    /// <summary>
    /// 获取单元格的值
    /// </summary>
    /// <param name="cell"></param>
    /// <returns></returns>
    private string getCellValue(ICell cell)
    {
        string value = null;
        if (cell != null)
        {
            value = cell.ToString();
        }
        return value;
    }

    /// <summary>
    /// 设置元表table
    /// </summary>
    /// <param name="sheet"></param>
    /// <param name="str"></param>
    private void setMetatable(ISheet sheet,ref string str,bool isArrayTable = false)
    {
        string func = @"{0}.__index = {1}; 
for k,v in pairs({2}) do
    setmetatable(v, {3});
end
{4}.__newindex = function ()
     error(""no can Attemt New Attribute"");
end";
        if (isArrayTable == true)
        {
            func = @"{0}.__index = {1}; 
for k,t in pairs({2}) do
    for i,v in ipairs(t) do
       setmetatable(v, {3});
    end
end
{4}.__newindex = function ()
     error(""no can Attemt New Attribute"");
end";
        }
        func = string.Format(func, defultTableName, defultTableName, sheet.SheetName, defultTableName, defultTableName);
        str = str + func;
        str += "\nreturn "+ sheet.SheetName+"; ";
    }
    /// <summary>
    /// 解析成Lua 数组
    /// </summary>
    /// <param name="sheet"></param>
    /// <returns></returns>
    public string parseToLuaArray(ISheet sheet,string jsonPath)
    {
        if (sheet == null)
        {
            return "";
        }
        initDefultValue(sheet);
        string str = defultTableStr + "local " + sheet.SheetName + "={\n\t";
        int rowCount = sheet.LastRowNum; //最后
        // 0 行描述  1行字段类型 2行字段名 3行默认值
        IRow rowValueType = sheet.GetRow(1); //值类型
        IRow rowKey = sheet.GetRow(2); //键

        string result = "";
        for (int i = 4; i <= rowCount; i++) //行
        {
            IRow row = sheet.GetRow(i);
            ICell idCell = row.GetCell(0);
            string id = getCellValue(idCell);
        
            string content = "";
            if (string.IsNullOrEmpty(id)) //id 是空的  这行数值还是属于上一个的
            {
                content+= ",\n\t\t{";
            }
            else //该id段结束
            {
                content+= "\n\t\t{";
                if (i != 4)
                {
                    result += "\n\t}";
                    result += ",\n\t" + "[" + id + "]" + "={";
                }
                else
                {
                    result += "["+id+"]" + "={";
                }
            }
            for (int j = 1; j < rowKey.LastCellNum; j++) // 列
            {
                string key = null; //字段名
                string value = null;//
                string[] cellValue = getCellData(row, rowValueType, rowKey, j);
                key = cellValue[0];
                value = cellValue[1];
                if (string.IsNullOrEmpty(key) && string.IsNullOrEmpty(value))
                {
                    break;
                }
                if (defult[key] == value)
                {
                    continue;
                }
                content += string.Format("\n\t\t\t{0}={1},", key, value);
            }
            content += "\n\t\t}";
            result += content;
            if (i == rowCount)
            {
                result += "\n\t}";
            }
        }
        str += result;
        str += "\n}\n";
        setMetatable(sheet, ref str, true);
        writeText(jsonPath,str);
        return str;
    }
    private void writeText(string jsonPath,string str)
    {
        File.WriteAllText(jsonPath, str);
    }
    /// <summary>
    /// 添加字符串符号
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    private string addStringTips(string value)
    {
        value = "\""+value+"\"";
        return value;
    }
    /// <summary>
    /// 表格内容解析成字符串
    /// </summary>
    /// <param name="valueType"> 值类型</param>
    /// <param name="value"> 值</param>
    /// <returns>返回解析好的值</returns>
    private string parseValue(string valueType, string value)
    {
        string result = null;

        if (valueType == "string")
        {
            if (string.IsNullOrEmpty(value))
            {
                result = "\"\"";
            }
            else
            {
                result = "\""+value+"\"";
            }
        }
        else if (valueType == "boolean")
        {
            if (string.IsNullOrEmpty(value))
            {
                result = "false";
            }
            else
            {
                bool tem;
                if (Boolean.TryParse(value,out tem))
                {
                    result = tem.ToString().ToLower();
                }
                else
                {
                    result = "false";
                }
            }
        }
        else if (valueType == "number")
        {
            if (string.IsNullOrEmpty(value))
            {
                result = "0";
            }
            else
            {
                float tem;
                if (float.TryParse(value, out tem))
                {
                    result = tem.ToString();
                }
                else
                {
                    result = "0";
                }
            }
        }
        else if (valueType == "array")
        {
            if (string.IsNullOrEmpty(value))
            {
                result = "{}";
            }
            else
            {
                result = "{" + value + " }";
            }
        }else if (!string.IsNullOrEmpty(valueType))
        {
            Debug.LogError(">>>>未知值类型："+ valueType);
        }
        return result;
    }
}
